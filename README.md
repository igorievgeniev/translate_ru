# Установка
** Mozilla Firefox **
 
 1. Качаем плагин [greasemonkey](https://addons.mozilla.org/ru/firefox/addon/greasemonkey/)
 2. Заходим [сюда](https://bitbucket.org/igorievgeniev/translate_ru/raw/master/travian_ru.user.js) и Tampermonkey сама предложит установить скрипт
 3. Соглашаемся

---

** Google Chrome **

 1. Качаем плагин [Tampermonkey](https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo?utm_source=chrome-ntp-icon)
 2. Заходим [сюда](https://bitbucket.org/igorievgeniev/translate_ru/raw/master/travian_ru.user.js) и Tampermonkey сама предложит установить скрипт
 3. Жмем кнопку Install