// ==UserScript==
// @name         Travian Russian
// @description  Loads travian russian translation
// @version      0.1
// @author       Igor Ievgeniev
// @namespace    http://tr1x3.kingdoms.com/
// @match        http://*.kingdoms.com*
// @exclude      http://lobby.kingdoms.com*
// @exclude      http://*ru*.kingdoms.com*
// @downloadURL  https://bitbucket.org/igorievgeniev/translate_ru/raw/master/travian_ru.user.js
// @updateURL    https://bitbucket.org/igorievgeniev/translate_ru/raw/master/travian_ru.user.js
// @run-at       document-idle
// @grant        none
// ==/UserScript==

;(function(){
	'use strict';

	var postfix = config.version ? '?h=' + config.version : '';
	var links = [
		'http://cdn.traviantools.net/game/0.75/lang/ru.js',
		'http://cdn.traviantools.net/game/0.75/lang/ru_quests_version2.js'
	];

	function createScript (src) {
		var script = document.createElement('script');
		script.type = 'text/javascript';
		script.src = src + postfix;
		document.head.appendChild(script);
	}

	function setScripts (urls) {
		for (var i = 0, l = urls.length; i < l; i++) {
			createScript(urls[i]);
		}
	}

	function interval (checkCondition, callback, delay, maxTimes) {
		var _interval = function (d, mt) {
			return function(){
				if (mt-- > 0) {
					if (checkCondition()) {
						try {
							callback.call(null);
						} catch(e){
							mt = 0;
							throw e.toString();
						}
					} else {
						setTimeout(_interval, d);
					}
				}
			};
		}(delay, maxTimes);
		setTimeout(_interval, delay);
	}

	interval(function(){
		return window.Travian && typeof window.Travian.txt === 'object' && Object.keys(window.Travian.txt).length;
	}, function(){
		setScripts(links);
	}, 100, 120);

})();